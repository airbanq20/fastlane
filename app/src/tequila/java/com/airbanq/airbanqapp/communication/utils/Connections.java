package com.airbanq.airbanqapp.communication.utils;

/**
 * Created by ZPT-PC-02 on 22/12/2015.
 */
public class Connections {

    public static final int POST_TASK = 1;
    public static final int GET_TASK = 2;
    // connection timeout, in milliseconds (waiting to connect)
    public static final int CONN_TIMEOUT = 120000;

    // socket timeout, in milliseconds (waiting for data)
    public static final int SOCKET_TIMEOUT = 120000;

/*	public static final String[] PERMISSION = new String[] { "basic_info", "user_birthday",
			"user_address", "user_mobile_phone", "user_location", "user_photos","email ",
			"read_friendlists" };*/

    public static final String[] PERMISSION = new String[] { "public_profile", "email",  "user_friends"};
    public static final String DEV = "https://prod2.airbanq.com/ABSWebApplication/service/abservice/";

    //public static final String DEV = "https://test.airbanq.com/ABSWebApplication/service/abservice/";
    //public static final String DEV = "http://192.168.1.113:8080/ABSWebApplication/service/abservice/";
    //public static final String DEV = "http://192.168.1.114:8080/ABSWebApplication/service/abservice/";


    public static String getUrl(){
        return DEV;
    }
}
